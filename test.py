# Python version
import sys
import scipy
import numpy
import matplotlib
import pandas
import sklearn



# Load libraries
import pandas
from pandas.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

urlX = "~/Downloads/Telegram Desktop/nlmk_public/nlmk_public/X_train.csv"
urlY = "~/Downloads/Telegram Desktop/nlmk_public/nlmk_public/y_train.csv"
#names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']

Xdataset = pandas.read_csv(urlX)
Ydataset = pandas.read_csv(urlY)

print(Xdataset.shape)
print(Ydataset.shape)

from sklearn.model_selection import train_test_split as train
X_train, X_test, y_train, y_test = train(Xdataset, Ydataset, test_size=0.6)

# from sklearn.ensemble import RandomForestClassifier
# clf = RandomForestClassifier(n_estimators=100, n_jobs=-1)
# clf.fit(Xdataset, Ydataset)
# print(clf.score(X_test, y_test))

from sklearn.ensemble import RandomForestClassifier
clf = RandomForestClassifier(n_estimators=100, n_jobs=-1)
clf.fit(X_train, y_train)
print(clf.score(X_test, y_test))