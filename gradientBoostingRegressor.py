import pandas as pd
import numpy as np
from sklearn.ensemble import GradientBoostingRegressor

# Loss function to be optimized
loss = 'ls'

# Number of trees used in the boosting process
n_estimators = 500

# Maximum depth of each tree
max_depth = 8

# Minimum number of samples per leaf
min_samples_leaf = 1

# Minimum number of samples to split a node
min_samples_split = 2

# Maximum number of features to consider for making splits
max_features = 'auto'

# Define the grid of hyperparameters to search
hyperparameter_grid = {'loss': loss,
                       'n_estimators': n_estimators,
                       'max_depth': max_depth,
                       'min_samples_leaf': min_samples_leaf,
                       'min_samples_split': min_samples_split,
                       'max_features': max_features}

# Create the model to use for hyperparameter tuning
clf = GradientBoostingRegressor(loss=loss, n_estimators=n_estimators, max_depth=max_depth,
                                min_samples_leaf=min_samples_leaf, min_samples_split=min_samples_split,
                                max_features=max_features)
# Read csv
x_traine_df = pd.read_csv('nlmk_public/nlmk_public/X_train.csv')
y_traine_df = pd.read_csv('nlmk_public/nlmk_public/y_train.csv')
x_test_df = pd.read_csv('nlmk_public/nlmk_public/X_test.csv')

# Replace null value with column average
x_traine_df = x_traine_df.fillna(x_traine_df.mean())
y_traine_df = y_traine_df.fillna(y_traine_df.mean())
x_test_df = x_test_df.fillna(x_test_df.mean())

# Fit the model on the training data
print("Start fit")
fit = clf.fit(x_traine_df, y_traine_df)

# Make predictions on the test data
print("Start predictions")
predictions = clf.predict(x_test_df)

np.savetxt("Y_test.csv", predictions, delimiter="\n")

traine_predictions = clf.predict(x_traine_df)
shape = y_traine_df.shape[0]
score = 0
for i in range(shape):
    score += abs(y_traine_df.iloc[:, 0][i] - predictions[i])

print(score / shape)
