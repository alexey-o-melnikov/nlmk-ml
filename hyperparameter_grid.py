import pandas as pd
import math
import numpy as np
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import RandomizedSearchCV

# Loss function to be optimized
loss = ['ls']

# Number of trees used in the boosting process
n_estimators = [500]

# Maximum depth of each tree
max_depth = [3]

# Minimum number of samples per leaf
min_samples_leaf = [1]

# Minimum number of samples to split a node
min_samples_split = [2]

# Maximum number of features to consider for making splits
max_features = ['auto']

# Define the grid of hyperparameters to search
hyperparameter_grid = {'loss': loss,
                       'n_estimators': n_estimators,
                       'max_depth': max_depth,
                       'min_samples_leaf': min_samples_leaf,
                       'min_samples_split': min_samples_split,
                       'max_features': max_features}

# Create the model to use for hyperparameter tuning
model = GradientBoostingRegressor(random_state=42)

# Set up the random search with 4-fold cross validation
random_cv = RandomizedSearchCV(estimator=model,
                               param_distributions=hyperparameter_grid,
                               cv=4, n_iter=5,
                               scoring='neg_mean_absolute_error',
                               n_jobs=-1, verbose=1,
                               return_train_score=True,
                               random_state=42)

#####################################################################
x_traine_df = pd.read_csv('nlmk_public/nlmk_public/X_train.csv')
y_traine_df = pd.read_csv('nlmk_public/nlmk_public/y_train.csv')
x_test_df = pd.read_csv('nlmk_public/nlmk_public/X_test.csv')

x_traine_df = x_traine_df.fillna(x_traine_df.mean())
y_traine_df = y_traine_df.fillna(y_traine_df.mean())
x_test_df = x_test_df.fillna(x_test_df.mean())

#####################################################################

# Fit on the training data
random_cv.fit(x_traine_df, y_traine_df)

# Find the best combination of settings
print(random_cv.best_estimator_)
GradientBoostingRegressor(loss='lad', max_depth=5,
                          max_features=None,
                          min_samples_leaf=6,
                          min_samples_split=6,
                          n_estimators=500)


# GradientBoostingRegressor(alpha=0.9, criterion='friedman_mse', init=None,
#                           learning_rate=0.1, loss='ls', max_depth=3,
#                           max_features='auto', max_leaf_nodes=None,
#                           min_impurity_decrease=0.0, min_impurity_split=None,
#                           min_samples_leaf=1, min_samples_split=2,
#                           min_weight_fraction_leaf=0.0, n_estimators=500,
#                           n_iter_no_change=None, presort='auto', random_state=42,
#                           subsample=1.0, tol=0.0001, validation_fraction=0.1, verbose=0,
#                           warm_start=False)
