import pandas as pd
import matplotlib.pyplot as plt

URL = "nlmk_public/nlmk_public/X_train.csv"
# Original column's names.
SPEED1 = "speed1"

# New column's names.
AGE_GROUP_COL = "AgeGroup"

# Other constants.
HEAD_ROWS_TO_SHOW = 15

df = pd.read_csv(URL)
var = df[:HEAD_ROWS_TO_SHOW]

# print(var)

print(df.shape)

print(df.dtypes)

print(df.info())

# print(df[SPEED1].value_counts())

print(df.loc[:5, [SPEED1, 'speed2']])
# ||
# df.iloc[:6Z, 0:2].hist()
# plt.show()

df.iloc[:, 0:1].plot()
plt.show()



